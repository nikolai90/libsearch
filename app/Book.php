<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    
    public function book_html() {
        return $this->hasOne('App\BookHtml', 'book_id', 'id');
    }
    
    public function author() {
        return $this->hasOne('App\Author', 'id', 'author_id');
    }
}
