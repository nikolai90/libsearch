<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class UserProfileUpdateController extends Controller
{
    public function __invoke(Request $request) {
        $user = $request->user();
        if ($request->email || $request->password) {
            
            $email = $user ->email;
            
            if (!auth()->validate([
                "email" => $email,
                "password" => $request->password,
            ])){
                return response( ['errors' =>  ['Неправильный пароль']  ], 401);
            } 
        }

        if ($request->name) {
            $user->name = $request->name;
        }

        if ( $request->passwordNew ) {
            $user->password = Hash::make($request->passwordNew);
        }

        if ( $request->email ) {
            $user->email = $request->email;
        }

        $user->save();

        return response()->json([
            'email' => $user->email,
            'name' => $user->name,
            'avatar' => $user->avatar
        ]);
    }
}
