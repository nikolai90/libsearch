<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Book;
use App\Booktype;
use App\BookHtml;
class BookController extends Controller
{
    public function getall(Request $request) {
        
        if ( isset($request->regex) ) 
        {
            $regex = $request->regex;
            $columnName = $request->columnName;
            $data = Book::where($columnName , '~*', $regex)->get();
        } else {
            $data = Book::all();
        }
        
         return response()->json($data);
    }

    public function get(Request $request) {
        $book_id = $request->id;
        $html = BookHtml::where('book_id' , '=', $book_id)->get();
        dd($html);
        if ($html) {
            return view('book', ['html' => $html]);
        }

    }

    public function getHtml(Request $request) {
        return Book::with('book_html')->where('id' , '=', $request->id)->get();
    }

    public function getBooktypes() {
        $data = Booktype::all();
        return response()->json($data);
    }

    public function bookscount() {
        $count = DB::table('books')->count();
        return $count;
    }
}
