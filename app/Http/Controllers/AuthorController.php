<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Author;

class AuthorController extends Controller
{
    public function getall(Request $request) {
        
        if ( isset($request->regex) ) 
        {
            $regex = $request->regex;
            $columnName = $request->columnName;
            $data = Author::where($columnName , '~*', $regex)->get();
        } else {
            $data = Author::all();
        }
        
         return response()->json($data, 200);
    }
}
