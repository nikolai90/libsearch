<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quote;
class QuoteController extends Controller
{
    public function store(Request $request) {
        $quote = new Quote;

        $quote->quote_text = $request->quote_text;
        $quote->book_id = $request->book_id;
        $quote->author_id = $request->author_id;
        $quote->user_id = $request->user_id;

        $quote->save();
    }

    public function getAll(Request $request) {
        return Quote::with(['author', 'book'])->get();
    }

    public function deleteQuote(Request $request) {
    
        $res = Quote::find($request->id);

        if ($res) return response('Success', 200);
        
    }
}
