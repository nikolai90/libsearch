<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;

class SearchtextController extends Controller
{
    public function __invoke(Request $request) {
        $offset = $request->offset;
        $limit = $request->limit;
        $query = $request->q;

        $books = Book::skip($offset)->take($limit)->get();
  
        if (count($books) == 0) return [];
        
        $handle = fopen('data/booksconcat.txt', 'r');
        $result = [];
        $i = 0;

        foreach($books as $book) {
      
          $bookCoords = explode(':', $book['textpos']);
  
          $bookStart = $bookCoords[0];
          $bookLength = $bookCoords[1];
  
          fseek($handle, $bookStart);
          if ($bookLength == 0) continue;
          $bookText = fread($handle, $bookLength);
          
          $r = $this->doSearchText($bookText, $query);
          if ($r > 0) {
            $book['resultSearchCount'] = $r;
            $result[] = $book;
          }
        }
  
        return $result;
    }

    public function doSearchText($str, $searchValue) 
    {
        $q = mb_strtolower($searchValue);
        return substr_count($str, $q );
    }
}
