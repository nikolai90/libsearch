<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SignInController extends Controller
{
    public function __invoke(Request $request) {
        
        $authData = $request->only('email', 'password');
        
        if (!$token = auth()->attempt( $authData ) ) {
            return response( ['errors' => ['auth' => ['Неправильный логин или пароль'] ] ], 401);
        }

        return response()->json([
            'token' => $token
        ]);

    }
}
