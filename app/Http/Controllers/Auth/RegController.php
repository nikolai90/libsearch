<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegController extends Controller
{
    public function __invoke(Request $request) {
        
        $validatedData = $request->validate([
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
        ],
        [
            'email.unique' => 'Аккаунт с такой почтой уже зарегистрирован',
       ]);
        
        return User::create([
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'name' => 'User'
        ]);
    }
}
