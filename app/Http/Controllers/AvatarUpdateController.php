<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AvatarUpdateController extends Controller
{
    public function __invoke(Request $request) {
        $request->validate([
            'file' => 'required|mimes:png,jpg,jpeg|max:2048',
        ]);
  
        $fileName = time().'.'.$request->file->extension();
        
        $user = $request->user();
        $user->avatar = $fileName;
        $user->save();
   
        $request->file->move(public_path('uploads'), $fileName);
            
        return response()->json([
            'message' => 'You have successfully upload file.',
            'file' => $fileName,
            'id' => $user->id
        ]);
    }
}
