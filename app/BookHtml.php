<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookHtml extends Model
{

    protected $table = 'books_html';
}
