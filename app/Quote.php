<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function book() {
        return $this->hasOne('App\Book', 'id', 'book_id');
    }

    public function author() {
        return $this->hasOne('App\Author', 'id', 'author_id');
    }
}
