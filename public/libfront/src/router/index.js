import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store/';
Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "books" */ '../views/Books')
  },
  {
    path: '/authors',
    name: 'Authors',
    component: () => import(/* webpackChunkName: "authors" */ '../views/Authors')
  },
  {
    path: '/booktypes',
    name: 'Booktypes',
    component: () => import(/* webpackChunkName: "booktypes" */ '../views/Booktypes') 
  },
  {
    path: '/login',
    name: 'UserLogin',
    component: () => import(/* webpackChunkName: "login" */ '../views/UserLogin')
  },
  {
    path: '/reg',
    name: 'UserReg',
    component: () => import(/* webpackChunkName: "reg" */ '../views/UserReg')
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import(/* webpackChunkName: "profile" */ '../views/Profile')
  },
  {
    path: '/search',
    name: 'Search',
    component: () => import(/* webpackChunkName: "search" */ '../views/SearchText')
  },
  {
    path: '/books/:id',
    name: 'Book',
    component: () => import(/* webpackChunkName: "book" */ '../views/Book')
  },
  {
    path: '/savedquotes',
    name: 'Quotes',
    component: () => import(/* webpackChunkName: "quotes" */ '../views/Quotes')
  }
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {

  const authed = store.getters['auth/authenticated'];
  const needAuthPaths = ['/profile', '/savedquotes'];

  needAuthPaths.forEach( (path) => {
    if (to.path == path && !authed) next({name: 'UserLogin'});
  })
  
  next();
})

export default router
