import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import Vuelidate from 'vuelidate'
require('@/store/subscriber')
import './styles/main.scss'

Vue.use(Vuelidate)
Vue.config.productionTip = false

axios.defaults.baseURL = process.env.VUE_APP_API_URL;


store.dispatch('auth/attempt', localStorage.getItem('token')).then( () => {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
})

