
import axios from 'axios'
export default {
  namespaced: true,
  state: {
    token: null,
    user: null,
  },
  mutations: {
    SET_TOKEN(state, token) {
      state.token = token;
    },
    SET_USER(state, user) {
      state.user = user;
    },
    SET_AVATAR_IMAGE(state, path) {
      state.user.avatar = path;
    }
  },
  getters: {
    authenticated (state) {
      return state.token != null && state.user != null;
    },
    user(state) {
      return state.user;
    },
    avatar(state) {
      if (!state.user.avatar) return false;
      return `${process.env.VUE_APP_FILES_UPLOAD_PATH}${state.user.avatar}`;
    }
  },
  actions: {
    async signIn ({ dispatch }, creditionals) {
      let response = await axios.post('auth/signin', creditionals);

      return dispatch('attempt', response.data.token);
    },
    async attempt({ commit }, token) {
      if (!token) return;

      commit('SET_TOKEN', token)

      try {
        let response = await axios.get('auth/me', {
          headers: {
            'Authorization': 'Bearer ' + token,
          }
        });

        commit('SET_USER', response.data);
      } catch (e) {
        commit('SET_TOKEN', null);
        commit('SET_USER', null);
      }
    },
    signOut({commit}) {
      return axios.post('auth/signout').then(()=>{
        commit('SET_TOKEN', null);
        commit('SET_USER', null);
      })
    },
    newUser({dispatch}, creditionals) {
      return axios.post('auth/reg', creditionals).then(() => {
        dispatch('signIn', creditionals)
      })
    },
    async uploadAvatarFile({ commit }, file) {
      const formData = new FormData();

      formData.append("file", file);
      const res = await axios.post('/avatarupdate', formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
      })


      commit('SET_AVATAR_IMAGE', res.data.file)
    },
    async updateUserProfile({commit}, data) {
      const res = await axios.post('/userupdate', data);

      commit('SET_USER', res.data);
    }
  },
}
