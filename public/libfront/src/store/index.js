import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import pageAuthors from './pages/pageAuthors'
import pageBooks from './pages/pageBooks'
import pageBooktypes from './pages/pageBooktypes'
import pageSearch from './pages/pageSearch'
import pageBook from './pages/pageBook'
import pageQuotes from './pages/pageQuotes'

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    searchquery: '',
  },
  modules: {
    auth,
    pageAuthors,
    pageBooks,
    pageBook,
    pageBooktypes,
    pageSearch,
    pageQuotes
  }
})
