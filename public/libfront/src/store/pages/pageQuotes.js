import axios from 'axios'
export default {
  name: 'pageQuotes',
  namespaced: true,
  state: {
    quotes: null,
    quotesPending: {},
  },
  getters: {
    quotes(state) {
      return state.quotes;
    },
    quotesPending(state) {
      return Object.keys(state.quotesPending);
    }
  },
  mutations: {
    SET_QUOTES(state, payload) {
      state.quotes = payload;
    },
    ADD_QUOTE_PENDING(state, id) {
      const obj = Object.assign({}, state.quotesPending)
      obj[id] = true;
      state.quotesPending = Object.assign({}, obj)
    },
    REMOVE_QUOTE_PENDING(state, id) {
      const obj = Object.assign({}, state.quotesPending)
      delete obj[id];
      state.quotesPending = Object.assign({}, obj)
    },
  },
  actions: {
    async fetchQuotes ({commit}) {
      const res = await axios.get('/quotes');
      commit('SET_QUOTES', res.data);
    },
    async removeQuote({state, commit}, i) {
      const id = state.quotes[i].id;
      commit('ADD_QUOTE_PENDING', id);
      const res = await axios.post('deletequote', {id});

      if (res.status == 200) {
        state.quotes.splice(i, 1);
      }
      commit('REMOVE_QUOTE_PENDING', id);
    }
  },
  modules: {

  }
}