export const checkLetter = (item, state) => {
  if (state.currentLetterPage == 'Все') return true;
  const name = item.name.toLowerCase();
  return name[0] == state.currentLetterPage.toLowerCase();
}

export const checkSearch = (item, rootState) => {
  const name = item.name.toLowerCase();
  const q = rootState.searchquery.toLowerCase();
  return name.indexOf(q) != -1;
}

export const checkBookType = (item, state) => {
  const itemType = item.type.toLowerCase().trim();
  const chosenType = state.booktype.toLowerCase().trim();
  if (chosenType == '') return true;
  return itemType == chosenType
}

export const checkAuthor = (item, state) => {
  if (state.authorID == '') return true;
  return item.author_id == state.authorID
}