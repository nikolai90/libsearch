import axios from 'axios'
export default {
  name: 'pageAuthors',
  namespaced: true,
  state: {
    booktypes: null,
  },
  getters: {
    booktypes(state) {
      return state.booktypes;
    }
  },
  mutations: {
    SET_BOOK_TYPES(state, payload) {
      state.booktypes = payload;
    }
  },
  actions: {
    async fetchBooktypes({ commit }) {
      const res = await axios.post('booktypes/');
      commit('SET_BOOK_TYPES', res.data);
    }
  },
  modules: {

  }
}