import axios from 'axios'
import {checkLetter, checkSearch, checkBookType, checkAuthor} from './utils/filterhelpers';
import fStats from '../../const/FetchStatus';
export default {
  name: 'pageAuthors',
  namespaced: true,
  state: {
    booksList: null,
    booksLIstFetchedFull: false,
    currentLetterPage: 'А',
    booktype: '',
    authorID: '',
    fetchedbooks: {},
    fetchStatus: fStats.ready,
  },
  getters: {
    filteredBooksList(state, getters, rootState) {
      if (!state.booksList) return []

      return state.booksList.filter( item =>
        checkLetter(item, state) &&
        checkSearch(item, rootState) &&
        checkBookType(item, state) &&
        checkAuthor(item, state)
      )
    },
    currentLetterPage(state) {
      return state.currentLetterPage
    },
    booktype(state) {
      return state.booktype
    },
    fetchStatus(state) {
      return state.fetchStatus
    }
  },
  mutations: {
    SET_BOOK_TYPE(state, payload) {
      state.booktype = payload;
    },
    SET_CURRENT_LETTER_PAGE(state, payload) {
      state.currentLetterPage = payload;
    },
    SET_BOOKS_LIST(state, payload) {
      state.booksList = payload;
    },
    SET_AUTHOR_ID(state, payload) {
      state.authorID  = payload;
    },
    SET_FETCH_STATUS(state, payload) {
      state.fetchStatus = payload;
    }
  },
  actions: {
    async fetchBooks({ state, commit, dispatch }) {
      if (state.fetchedbooks['Все']) return;

      let books = [];
      
      if (state.fetchedbooks[state.currentLetterPage]) {
        books = state.fetchedbooks[state.currentLetterPage];
      } else {
        commit('SET_FETCH_STATUS', fStats.fetching);
        books = await dispatch('sendFetchBooks');
      }

      commit('SET_FETCH_STATUS', fStats.ready);
      commit('SET_BOOKS_LIST', books);
      state.fetchedbooks[state.currentLetterPage] = books;
    },
    async sendFetchBooks({state}) {
      let res;
      if ( state.currentLetterPage == 'Все' ) {
        res = await axios.post('books/');
      } else {
        res = await axios.post('books/', {
          'columnName' : 'name',
          'regex': `^${state.currentLetterPage}`,
        });
      }
      return res.data;
    }
  },
  modules: {

  }
}