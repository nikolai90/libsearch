import axios from 'axios'
import {checkLetter, checkSearch} from './utils/filterhelpers';
export default {
  name: 'pageAuthors',
  namespaced: true,
  state: {
    authorsList: null,
    currentLetterPage: 'А',
  },
  getters: {
    filteredAuthorsList(state, getters, rootState) {
      if (!state.authorsList) return []

      return state.authorsList.filter( item =>
        checkLetter(item, state) &&
        checkSearch(item, rootState)
      )
    },
    currentLetterPage(state) {
      return state.currentLetterPage;
    }
  },
  mutations: {
    SET_CURRENT_LETTER_PAGE(state, payload) {
      state.currentLetterPage = payload;
    }
  },
  actions: {
    async fetchAuthors({ state }) {
      
      const regExfilter = {
        'columnName' : 'name',
        'regex': `^${state.currentLetterPage}`,
      }
      
      const res = await axios.post('authors/', regExfilter);
      state.authorsList = res.data;

       axios.post('authors/').then(res => {
         state.authorsList = res.data;
       });
       
    }
  },
  modules: {

  }
}