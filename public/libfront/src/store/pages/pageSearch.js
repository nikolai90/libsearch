import Vue from 'vue';
// import axios from 'axios';

export default {
  name: 'pageSearch',
  namespaced: true,
  state: {
    booksCountFetched: 0,
    docs: [],
    resPageCurrent: 1,
    resPageLength: 50,
    numFound: null,
    fetching: false,
    query: '',
  },
  getters:{
    paginationCount(state) {
      if (!state.numFound) return null;
      return Math.ceil(state.numFound / state.resPageLength);
    },
  },
  actions:{
    onStartSearching({dispatch, state} ) {
      state.fetching = true;
      dispatch('fetchSearchResult')
        .then( () => state.fetching = false );
    },
    fetchSearchResult({state}) {
      return fetch(`${process.env.VUE_APP_SOLR_URL}solr/books/select?fl=author_id,book_id,book_name,wordsCount:termfreq(text,"${state.query}")&sort=termfreq(text,"${state.query}")+desc&q=text:${state.query}&rows=${state.resPageLength}&start=${state.resPageLength * state.resPageCurrent}`)
        .then((j) => j.json())
        .then((a) => {
          const {numFound, docs} = a.response;
          state.numFound = numFound;
          state.docs = docs;
        })
    },
    onChangeCurrentPageNum({state, dispatch}, pageNum) {
      Vue.set(state, 'resPageCurrent', pageNum);
      dispatch('onStartSearching');
    }
  },
  mutations:{
    SET_SEARCH_QUERY(state, payload) {
      state.query = payload;
    }
  },
}