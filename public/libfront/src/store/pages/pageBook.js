import axios from 'axios'
import {isElementInViewport} from '../../utils/fns';
export default {
  name: 'pageBook',
  namespaced: true,
  state: {
    book: null,
    searchQuery: '',
    matchCurrentIndex: 1,
    matchesTotal: 0,
    bookHtmlWithMarkedMatches: null,
  },
  mutations: {
    SET_BOOK(state, payload) {
      state.book = payload;
    },
    SET_SEARCH_PANEL_VALUE(state, payload) {
      state.searchQuery = payload;
    },
    SET_MATCH_CURRENT_INDEX(state, payload) {
      state.matchCurrentIndex = payload;
    },
    SET_HTML_WITH_MARKED_MATCHES(state, payload) {
      state.bookHtmlWithMarkedMatches = payload;
    },
    SET_MATCHES_TOTAL(state, payload) {
      state.matchesTotal = payload;
    }
  },
  getters: {
    book(state) {
      return state.book;
    },
    bookHtml(state) {
      if (state.bookHtmlWithMarkedMatches) return state.bookHtmlWithMarkedMatches;
      return state.book.book_html.text;
    },
    matchCurrentIndex(state) {
      return state.matchCurrentIndex;
    },
    matchesTotal(state) {
      return state.matchesTotal;
    },
    searchQuery(state) {
      return state.searchQuery;
    }
  },
  actions: {
    async fetchBook({commit}, id) {
      const res = await axios.post(`/books/${id}`);
      commit('SET_BOOK', res.data[0]);
    },
    async sendQuoteSave(_, payload) {
      const res = await axios.post('savequote', payload);
      return res;
    },
    onSetSearchValue({commit, dispatch, state}, payload) {
      commit('SET_SEARCH_PANEL_VALUE', payload);
      commit('SET_MATCH_CURRENT_INDEX', 1);
      dispatch('buildHtmlWithMarkedMatches');
      if (state.matchesTotal > 0) {
        dispatch('handleScrollBehaviour');
      }
    },
    buildHtmlWithMarkedMatches({commit, state}) {
      const regex = new RegExp(`(?<=[\\s,.:;"']|^)${state.searchQuery}(?=[\\s,.:;"']|$)`, 'g');
      let html = state.book.book_html.text;
      let nth = 0;
      html = html.replace(regex, match => {
        nth++;
        const id = `id="lb-marked-text-id-${nth}"`;
        let replaceTo = `<span ${id}" class="text-marked">${match}</span>`;
        if (nth == state.matchCurrentIndex) replaceTo = `<span ${id}" class="text-marked yellow">${match}</span>`;
        return replaceTo
      });
      commit('SET_MATCHES_TOTAL', nth);
      commit('SET_HTML_WITH_MARKED_MATCHES', html);
    },
    handleCurrentMatchBehaviour({state, dispatch}) {
      setTimeout(() => {
        const nodes = document.querySelectorAll('.text-marked');
        nodes.forEach(n => {
          n.classList.remove('yellow');
        })

        const id = `lb-marked-text-id-${state.matchCurrentIndex}`;
        const currentMatch = document.getElementById(id);
        currentMatch.classList.add('yellow');
        dispatch('handleScrollBehaviour');
      }, 0)
    },
    onSetMatchCurrentIndex({commit, dispatch, state}, payload) {
      if (state.matchesTotal == 0) return;
      commit('SET_MATCH_CURRENT_INDEX', payload);
      dispatch('handleCurrentMatchBehaviour');
    },
    handleScrollBehaviour({state}) {
      setTimeout(() => {
        const id = `lb-marked-text-id-${state.matchCurrentIndex}`;
        const currentMatch = document.getElementById(id);
        if (!isElementInViewport(currentMatch)) {
          currentMatch.scrollIntoView();
          window.scrollBy(0, -60);
        }
      })
    },
    getMarkedTextElementId() {

    }
  },
  modules: {

  }
}