export default {
  namespaced: true,
  state: {
    searchQuery: null,
    tableDataVariableName: null,
  },
  mutations: {
    SET_CURRENT_TABLE_DATA_VAR_NAME(state, payload) {
      state.tableDataVariableName = payload;
    }
  },
  getters: {
    tableData() {
      return [1, 2, 3];
    }
  },
  actions: {

  },
  modules: {

  }
}