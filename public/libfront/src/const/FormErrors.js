export default {
  required: 'Заполните поле',
  mailexist: 'Аккаунт с такой почтой уже зарегистрирован',
  InvalidEmail: 'Неправильный адрес почты',
  passwordShort: 'Пароль должен содержать не менее 6 символов',
  passwordNotSame: 'Пароль не совпадает'
}