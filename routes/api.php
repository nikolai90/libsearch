<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function() {

    Route::post('signin', 'SignInController');
    Route::post('signout', 'SingOutController');
    Route::get('me', 'MeController');
    Route::post('reg', 'RegController');

});


Route::post('books', 'BookController@getall');
Route::post('bookscount', 'BookController@bookscount');
Route::post('booktypes', 'BookController@getbooktypes');
Route::post('searchtext', 'SearchtextController');
Route::post('books/{id}', 'BookController@getHtml');
Route::post('authors', 'AuthorController@getall');

Route::post('avatarupdate', 'AvatarUpdateController');
Route::post('userupdate', 'UserProfileUpdateController');

Route::post('savequote', 'QuoteController@store');
Route::get('quotes', 'QuoteController@getAll');
Route::post('deletequote', 'QuoteController@deleteQuote');


